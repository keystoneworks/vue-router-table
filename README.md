# vue-router-table

The goal of the project is to sync the routes between [Vue Router](https://router.vuejs.org/) and NodeJS servers.

So when using SSR and we know that we will be returning a 300 or 404 HTTP response, there is no need to call the Vue SSR Server Plugin.

For example, one of my projects has hundreds of redirects, thanks to many years of changing features and frameworks. I don't want to force my visitors to download the entire redirect structure.

## Features

Some high level features that I want to support.

- [ ] Vue admin pages for CRUD operations and maintaining the routes. Similar to Django's Admin interface for their redirects app.
- [ ] Have a concept of multiple sites.
- [ ] Integration with [ExpressJS](https://expressjs.com/)
- [ ] Integration with [KOA](https://koajs.com/)
- [ ] Store routes in a relational or NoSQL database, or flat static JSON stored on AWS S3 or GCP Storage
- [ ] Web worker interface for installation on [Cloudflare](https://www.cloudflare.com/products/cloudflare-workers/)
- [ ] Provide a fallback route that calls an API that queries for a redirect
- [ ] Keep track of versions
- [ ] Possibly trigger a new build, when routes are changed

## Versioning

The versioning scheme we use is [SemVer](https://semver.org/).
